from wordcloud import WordCloud
import numpy as np
from PIL import Image
import os

with open('/usr/share/dict/american-english') as f:
    wordList = f.read().splitlines()
    
text = " ".join(wordList)
print(text)

src_dir = os.getcwd()
imagePath = src_dir + "/superman.png"
font = src_dir + "/SimHei.ttf"
resultPath = src_dir + "/output.png"

bg = np.array(Image.open(imagePath))
wc = WordCloud(
    mask=bg,  
    background_color="white", 
    max_font_size=40, 
    min_font_size=5, 
    max_words=5000,
    random_state=40,  
    font_path=font, 
).generate(text)
wc.to_file(resultPath)

